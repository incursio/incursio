package com.lionseal.incursio;

import org.andengine.engine.camera.ZoomCamera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.RepeatingSpriteBackground;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.batch.DynamicSpriteBatch;
import org.andengine.entity.sprite.batch.SpriteBatch;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.source.AssetBitmapTextureAtlasSource;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.ui.activity.SimpleLayoutGameActivity;

import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;

import com.lionseal.incursio.logic.Casilla;
import com.lionseal.incursio.logic.Ficha;
import com.lionseal.incursio.logic.Tablero;
import com.lionseal.incursio.utils.AdMob;
import com.lionseal.incursio.utils.Sonidos;
import com.lionseal.incursio.utils.Texturas;

public class BoardScreen extends SimpleLayoutGameActivity implements OnDismissListener, OnCancelListener {

	// Constants
	private static final int CASILLA_SIZE = 300;
	private static final int FICHA_SIZE = 300;
	private static final int ROW_COUNT = 5;
	private static final int COLUMN_COUNT = 9;
	private static final float TABLERO_WIDTH = CASILLA_SIZE * COLUMN_COUNT;
	private static final float TABLERO_HEIGHT = CASILLA_SIZE * ROW_COUNT;

	// Camera settings
	private ZoomCamera camera;
	private float zoomFactor;
	private static float CAMERA_WIDTH;
	private static float CAMERA_HEIGHT;

	// Texturas
	private static final String CASILLA_PATH = "casilla.png";
	private static final String FICHA_P1_PATH = "p1.png";
	private static final String FICHA_P2_PATH = "p2.png";
	private static final int CASILLA_TEXTURE_COLUMNS = 9;
	private static final int FICHA_TEXTURE_COLUMNS = 6;
	private BitmapTextureAtlas texturasCasilla;
	private BitmapTextureAtlas texturasFichaP1;
	private BitmapTextureAtlas texturasFichaP2;
	private RepeatingSpriteBackground background;
	private Font font;

	// Sprites
	private AnimatedSprite[][] spritesCasillas = new AnimatedSprite[COLUMN_COUNT][ROW_COUNT];
	private AnimatedSprite[][] spritesFichas = new AnimatedSprite[2][5];

	// Fill
	private Scene scene;
	private Tablero tablero;
	private AdMob ads;
	private boolean pause = true;
	private boolean choose = true;

	protected void onCreate(Bundle b) {
		super.onCreate(b);
		ads = new AdMob(this);
	}

	protected void onStart() {
		super.onStart();
		ads.loadInterstitial();
	}

	protected void onResume() {
		super.onResume();
		if (choose)
			runOnUiThread(new Runnable() {
				public void run() {
					ObjectivoDialog d = new ObjectivoDialog(BoardScreen.this);
					d.show();
					choose = false;
				}
			});
	}

	public void objetivoSelected(int obj) {
		tablero.setiObjetivo(obj);
		// TODO
		String notice = obj > 20 ? getResources().getString(R.string.notice3) : obj > 10 ? getResources().getString(R.string.notice2) : getResources().getString(R.string.notice1);
		Toast.makeText(this, notice, Toast.LENGTH_LONG).show();
		pause = false;
	}

	@Override
	public EngineOptions onCreateEngineOptions() {
		DisplayMetrics d = getResources().getDisplayMetrics();
		CAMERA_WIDTH = d.widthPixels;
		CAMERA_HEIGHT = d.heightPixels;
		float minZoomY = CAMERA_HEIGHT / TABLERO_HEIGHT, minZoomX = CAMERA_WIDTH / TABLERO_WIDTH;
		zoomFactor = minZoomY < minZoomX ? minZoomY : minZoomX;
		camera = new ZoomCamera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
		final EngineOptions options = new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, new FillResolutionPolicy(), camera);
		return options;
	}

	@Override
	public void onCreateResources() {
		TextureManager tm = getTextureManager();
		TiledTextureRegion ttr;
		texturasCasilla = new BitmapTextureAtlas(tm, CASILLA_SIZE * CASILLA_TEXTURE_COLUMNS, CASILLA_SIZE);
		ttr = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(texturasCasilla, this, CASILLA_PATH, 0, 0, CASILLA_TEXTURE_COLUMNS, 1);
		Texturas.getInstance().putTiledTexture(Texturas.CASILLA, ttr);
		texturasCasilla.load();

		texturasFichaP1 = new BitmapTextureAtlas(tm, FICHA_SIZE * FICHA_TEXTURE_COLUMNS, FICHA_SIZE);
		ttr = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(texturasFichaP1, this, FICHA_P1_PATH, 0, 0, FICHA_TEXTURE_COLUMNS, 1);
		Texturas.getInstance().putTiledTexture(Texturas.FICHA_P1, ttr);
		texturasFichaP1.load();

		texturasFichaP2 = new BitmapTextureAtlas(tm, FICHA_SIZE * FICHA_TEXTURE_COLUMNS, FICHA_SIZE);
		ttr = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(texturasFichaP2, this, FICHA_P2_PATH, 0, 0, FICHA_TEXTURE_COLUMNS, 1);
		Texturas.getInstance().putTiledTexture(Texturas.FICHA_P2, ttr);
		texturasFichaP2.load();

		background = new RepeatingSpriteBackground(CAMERA_WIDTH, CAMERA_HEIGHT, getTextureManager(), AssetBitmapTextureAtlasSource.create(getAssets(), "background.png"),
				getVertexBufferObjectManager());

		Sonidos.getInstance().load(this, R.raw.ficha);
		Sonidos.getInstance().load(this, R.raw.invalido);

		FontFactory.setAssetBasePath("fonts/");
		final ITexture fontTexture = new BitmapTextureAtlas(getTextureManager(), 256, 256, TextureOptions.BILINEAR);
		font = FontFactory.createFromAsset(getFontManager(), fontTexture, this.getAssets(), "PWScratchy.ttf", 36, true, android.graphics.Color.WHITE);
		font.load();
	}

	@Override
	public Scene onCreateScene() {
		scene = new Scene();

		int tama�o = ROW_COUNT * COLUMN_COUNT;
		tablero = new Tablero(this);
		SpriteBatch sb;
		sb = new DynamicSpriteBatch(0, 0, texturasCasilla, tama�o, getVertexBufferObjectManager()) {
			@Override
			public boolean onUpdateSpriteBatch() {
				Casilla[][] casillas = tablero.getaCasillas();
				for (Casilla[] cc : casillas) {
					for (Casilla c : cc) {
						draw(getSprite(c));
					}
				}
				return true;
			}
		};
		scene.attachChild(sb);
		sb = new DynamicSpriteBatch(0, 0, texturasFichaP1, 5, getVertexBufferObjectManager()) {
			@Override
			public boolean onUpdateSpriteBatch() {
				Ficha[] fichas = tablero.getjJugadores()[0].getfFichas();
				for (Ficha f : fichas) {
					if (!f.geteEstado().equals(Ficha.Estado.MUERTA))
						draw(getSprite(0, Texturas.FICHA_P1, f));
				}
				return true;
			}
		};
		scene.attachChild(sb);
		sb = new DynamicSpriteBatch(0, 0, texturasFichaP2, 5, getVertexBufferObjectManager()) {
			@Override
			public boolean onUpdateSpriteBatch() {
				Ficha[] fichas = tablero.getjJugadores()[1].getfFichas();
				for (Ficha f : fichas) {
					if (!f.geteEstado().equals(Ficha.Estado.MUERTA))
						draw(getSprite(1, Texturas.FICHA_P2, f));
				}
				return true;
			}
		};
		scene.attachChild(sb);

		scene.setBackground(background);

		float minX = 0;
		float minY = 0;
		float maxX = TABLERO_WIDTH;
		float maxY = TABLERO_HEIGHT;
		camera.setBoundsEnabled(true);
		camera.setBounds(minX, minY, maxX, maxY);
		camera.setZoomFactor(zoomFactor);
		camera.setCenter(TABLERO_WIDTH / 2, (TABLERO_HEIGHT / 2));

		// createMenuScene();

		return scene;
	}

	private void createMenuScene() {
		final int score1 = tablero.getjJugadores()[0].getiPuntos(), score2 = tablero.getjJugadores()[1].getiPuntos();
		final String next = tablero.getjJugadores()[0].isbTurno() == tablero.getbTurnoInicial() ? getResources().getString(R.string.player1) : getResources().getString(R.string.player2);
		final boolean fin = tablero.isbFin();
		Log.d("asd", "" + fin);
		runOnUiThread(new Runnable() {
			public void run() {
				MenuDialog m = new MenuDialog(BoardScreen.this, score1, score2, next, fin);
				m.setOnDismissListener(BoardScreen.this);
				m.show();
			}
		});
	}

	protected AnimatedSprite getSprite(final Casilla c) {
		int x = c.getiPosicion()[0], y = c.getiPosicion()[1];
		if (spritesCasillas[x][y] == null) {
			float pX = x * CASILLA_SIZE, pY = y * CASILLA_SIZE;
			VertexBufferObjectManager vbom = getVertexBufferObjectManager();
			AnimatedSprite sprite = new AnimatedSprite(pX, pY, Texturas.getInstance().getTiledTexture(Texturas.CASILLA), vbom) {
				public boolean onAreaTouched(final TouchEvent event, final float x, final float y) {
					if (!pause && event.isActionUp()) {
						tablero.getmMovimiento().DetectarCaso(c);
						if (tablero.getmMovimiento().isbFinPartida())
							Log.d("Incursio", "P1: " + tablero.getjJugadores()[0].getiPuntos() + ";" + "P2: " + tablero.getjJugadores()[1].getiPuntos());
					}
					return true;
				}
			};
			spritesCasillas[x][y] = sprite;
			spritesCasillas[x][y].setCurrentTileIndex(getIndexTile(c));
			scene.registerTouchArea(sprite);
		}
		return spritesCasillas[x][y];
	}

	protected AnimatedSprite getSprite(int jugador, String textura, final Ficha f) {
		final int x = f.getiPosicion()[0], y = f.getiPosicion()[1];
		int i = f.getiNumero();
		if (spritesFichas[jugador][i] == null) {
			float pX = x * FICHA_SIZE, pY = y * FICHA_SIZE;
			VertexBufferObjectManager vbom = getVertexBufferObjectManager();
			spritesFichas[jugador][i] = new AnimatedSprite(pX, pY, Texturas.getInstance().getTiledTexture(textura), vbom);
			spritesFichas[jugador][i].setCurrentTileIndex(getIndexTile(f));
		}
		return spritesFichas[jugador][i];
	}

	private int getIndexTile(Casilla c) {
		int index = 0;
		if (c.isbPisable()) {
			index++;
			int x = c.getiPosicion()[0], y = c.getiPosicion()[1];
			if (y == 0) {
				switch (x) {
				case 2:
					index++;
					break;
				case 4:
					index += 2;
					break;
				case 6:
					index += 3;
				}
			} else if (y == 4) {
				switch (x) {
				case 2:
					index += 3;
					break;
				case 4:
					index += 2;
					break;
				case 6:
					index++;
				}
			}
			if (c.isbResaltada()) {
				index += 4;
			}
		}
		return index;
	}

	private int getIndexTile(Ficha f) {
		int index = 0;
		if (f.geteEstado().ordinal() < 6) {
			index = f.geteEstado().ordinal();
		}
		return index;
	}

	public void ChangeSprite(Casilla c) {
		int x = c.getiPosicion()[0], y = c.getiPosicion()[1];
		AnimatedSprite sprite = spritesCasillas[x][y];
		if (sprite != null) {
			sprite.setCurrentTileIndex(getIndexTile(c));
		}
	}

	public void ChangeSprite(Ficha f) {
		AnimatedSprite sprite = spritesFichas[f.getjJugador().getiJugador()][f.getiNumero()];
		if (sprite != null) {
			sprite.setCurrentTileIndex(getIndexTile(f));
		}
	}

	public void MovePiece(Ficha f, Casilla prev, Casilla next) {
		int x = next.getiPosicion()[0], y = next.getiPosicion()[1];
		AnimatedSprite sprite = spritesFichas[f.getjJugador().getiJugador()][f.getiNumero()];
		if (sprite != null) {
			float pX = x * FICHA_SIZE, pY = y * FICHA_SIZE;
			sprite.setPosition(pX, pY);
		}
	}

	public void ReLoadBoard() {
		createMenuScene();
		pause = true;
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		if (tablero.isbFin()) {
			pause = true;
			spritesCasillas = new AnimatedSprite[COLUMN_COUNT][ROW_COUNT];
			spritesFichas = new AnimatedSprite[2][5];
			mEngine.setScene(onCreateScene());
			runOnUiThread(new Runnable() {
				public void run() {
					ObjectivoDialog d = new ObjectivoDialog(BoardScreen.this);
					d.show();
				}
			});
			Sonidos.getInstance().play(this, R.raw.boton);
			ads.showInterstitial();
		} else {
			spritesFichas = new AnimatedSprite[2][5];
			pause = false;
			tablero.NuevaPartida();
			Sonidos.getInstance().play(this, R.raw.boton);
			ads.countDownAndShow();
		}
	}

	@Override
	protected int getLayoutID() {
		return R.layout.board_screen;
	}

	@Override
	protected int getRenderSurfaceViewID() {
		return R.id.board_rendersurfaceview;
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		Sonidos.getInstance().play(this, R.raw.invalido);
		finish();
	}

	public void onBackPressed() {
		Sonidos.getInstance().play(this, R.raw.invalido);
		super.onBackPressed();
	}
}
