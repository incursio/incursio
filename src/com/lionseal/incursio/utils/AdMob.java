package com.lionseal.incursio.utils;

import android.app.Activity;
import android.util.Log;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.lionseal.incursio.R;

/*
 * 
 * 
 * WORKS. NOT READY TO USE YET. ALSO YOU NEED TO KNOW SOMETHING ABOUT TESTING ADS. YOU. MUST. NOT. CLICK. THEM.
 * 
 * 
 * */
public class AdMob extends AdListener {
	private InterstitialAd mInterstitial;
	private Activity context;
	private int interval = 0;

	public AdMob(Activity c) {
		context = c;
		mInterstitial = new InterstitialAd(context);
		mInterstitial.setAdUnitId(c.getResources().getString(R.string.interstitial_id));
		mInterstitial.setAdListener(this);
	}

	public void loadInterstitial() {
		AdRequest adRequest;
		adRequest = new AdRequest.Builder().addTestDevice("BCF679236A815A6A12F8F0A81961DDAE").build();

		// adRequest = new AdRequest.Builder().build();
		mInterstitial.loadAd(adRequest);
	}

	public void showInterstitial() {
		context.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (mInterstitial.isLoaded()) {
					mInterstitial.show();
				}
			}
		});
	}

	public void countDownAndShow() {
		context.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (interval < 1) {
					if (mInterstitial.isLoaded()) {
						mInterstitial.show();
						interval = 2;
					}
				} else {
					interval--;
				}
			}
		});
	}

	public void onAdLoaded() {
	}

	public void onAdFailedToLoad(int errorCode) {
		String errorReason = "No reason.";
		switch (errorCode) {
		case AdRequest.ERROR_CODE_INTERNAL_ERROR:
			errorReason = "Internal error";
			break;
		case AdRequest.ERROR_CODE_INVALID_REQUEST:
			errorReason = "Invalid request";
			break;
		case AdRequest.ERROR_CODE_NETWORK_ERROR:
			errorReason = "Network Error";
			break;
		case AdRequest.ERROR_CODE_NO_FILL:
			errorReason = "No fill";
			break;
		}
		Log.d("ADMOB", "Ad failed to load: " + errorReason);
	}

	public void onAdOpened() {
	}

	public void onAdClosed() {
	}

	public void onAdLeftApplication() {
	}
}
