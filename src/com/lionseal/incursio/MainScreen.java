package com.lionseal.incursio;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.lionseal.incursio.utils.Sonidos;

public class MainScreen extends Activity {

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_screen);

		Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/PWScratchy.ttf");
		Button b = (Button) findViewById(R.id.button_play);
		b.setTypeface(tf);
		b = (Button) findViewById(R.id.button_rules);
		b.setTypeface(tf);
		b = (Button) findViewById(R.id.button_credits);
		b.setTypeface(tf);

		SharedPreferences prefs = getSharedPreferences("CONFIG", 0);
		boolean shouldPlay = prefs.getBoolean("sound", true);
		Sonidos.getInstance().setShouldPlay(shouldPlay);
		Sonidos.getInstance().load(this, R.raw.boton);
		Sonidos.getInstance().load(this, R.raw.invalido);
		setSoundIcon(shouldPlay);
	}

	public void clickPlay(View v) {
		Sonidos.getInstance().play(this, R.raw.boton);
		Intent i = new Intent(this, BoardScreen.class);
		startActivity(i);
	}

	public void clickRules(View v) {
		Sonidos.getInstance().play(this, R.raw.boton);
		Intent i = new Intent(this, RulesScreen.class);
		startActivity(i);
	}

	public void clickCredits(View v) {
		Sonidos.getInstance().play(this, R.raw.boton);
		Intent i = new Intent(this, CreditsScreen.class);
		startActivity(i);
	}

	public void clickSound(View v) {
		SharedPreferences prefs = getSharedPreferences("CONFIG", 0);
		boolean shouldPlay = !prefs.getBoolean("sound", true);
		Sonidos.getInstance().setShouldPlay(shouldPlay);
		prefs.edit().putBoolean("sound", shouldPlay).commit();
		setSoundIcon(shouldPlay);
		if (shouldPlay)
			Sonidos.getInstance().play(this, R.raw.boton);
	}

	private void setSoundIcon(boolean shouldPlay) {
		ImageButton b = (ImageButton) findViewById(R.id.sound);
		Drawable d;
		if (shouldPlay) {
			d = getResources().getDrawable(android.R.drawable.ic_lock_silent_mode_off);
		} else {
			d = getResources().getDrawable(android.R.drawable.ic_lock_silent_mode);
		}
		b.setImageDrawable(d);
	}

}
