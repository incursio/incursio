package com.lionseal.incursio;

import com.lionseal.incursio.utils.Sonidos;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

public class CreditsScreen extends Activity {
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.credits_screen);

		Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/PWScratchy.ttf");
		TextView t = (TextView) findViewById(R.id.credits);
		t.setTypeface(tf);
	}
	
	public void onBackPressed(){
		Sonidos.getInstance().play(this, R.raw.invalido);
		super.onBackPressed();
	}
}
